## Descripción de la prueba:

El reto consiste en demostrar conocimiento acerca de los componentes que conforman un sistema distribuido.

La solución consta de 3 bloques:
- Formulario de login (correo electrónico + password + link de recuperación de clave).
- Recepción de la respuesta del formulario y envío de la informacón para su gestión de forma asíncrona.
- Command Handler: proceso que recibe los datos del formulario y ejecuta la lógica de negocio.

Tabla de usuarios
```
| id       | 1            |
| username | test         |
| password | $password%   |
| email    | xxxx@xxx.xxx |
| status   | 1 (active)   |
```
Tarea

El código debe permitir:
1. Al indicar el nombre de usuario y hacer click en recuperar clave, un proceso asíncrono enviará un email al usuario.
2. El usuario se debe poder autenticar con los datos recibidos y obtener un mensaje acorde de autenticación satisfactoria o fallida.

El tiempo máximo para la entrega es de 5 días a partir de la recepción de la prueba.
Los lenguajes admitidos son, PHP y Javascript.


### Inicialización del proyecto

```
make init
```

##### Organización:

El proyecto consta de:

```
backend symfony
http://api.localhost/
./api
```
```
frontend reactJs
http://localhost:3000/
./app
```
 
##### Consideraciones:

Ya que el reto no especificaba la necesidad de persistir cambios en los datos se ha optado por utilizar un array en el mismo repositorio y así no alargar el proceso teniendo que dockerizar base de datos y ni crear migraciones.

De igual modo, al no requerirse más que la recepción de un email con datos que permitan realizar el proceso de autenticación, se ha optado por hardcodear la contraseña en el email para obviar la creación de un sistema de recuperación de contraseña con enlace caduco y el consecuente caso de uso que actualizara la contraseña.

Para la recepción de emails es necesaria la configuración del servidor SMTP. En las variables de entorno bastaría con añadir un correo y contraseña de gmail, así como activar la siguiente opción https://myaccount.google.com/u/2/lesssecureapps al estar trabajando en local.

También es necesario modificar la variable de entorno USER_MAIL para especificar el email donde se recibirán los correos.
