<?php

namespace App\Domain\Service;

use App\Domain\Model\Exception\UserNotFoundException;
use App\Domain\Model\Exception\WrongPassword;
use App\Domain\Model\User;
use App\Domain\Model\UserRepository;
use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;

class Authenticator
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $repository){
        $this->userRepository = $repository;
    }

    public function execute(Email $email, string $password)
    {
        $user = $this->findUser($email);
        $this->ensureCorrectPassword($user,$password);

        return $user;
    }

    private function findUser(Email $email): User
    {
        $user = $this->userRepository->findByEmail($email);
        if(null === $user){
            throw UserNotFoundException::fromName($email);
        }

        return $user;
    }

    private function ensureCorrectPassword(User $user,string $password): void
    {
        if(!$user->password()->verify($password)){
            throw WrongPassword::fromName($user->email());
        }
    }
}