<?php

namespace App\Domain\Service;

use App\Domain\Model\Exception\UserNotFoundException;
use App\Domain\Model\UserRepository;
use App\Domain\Model\ValueObject\Name;

class PasswordRecover
{
    private UserRepository $userRepository;
    private RecoverPasswordMessenger $recoverPasswordMessenger;

    public function __construct(UserRepository $repository, RecoverPasswordMessenger $recoverPasswordMessenger){
        $this->userRepository = $repository;
        $this->recoverPasswordMessenger = $recoverPasswordMessenger;
    }

    public function execute(Name $username)
    {
        $user = $this->userRepository->findByName($username);
        if(null === $user){
            throw UserNotFoundException::fromName($username);
        }
        $this->recoverPasswordMessenger->send($user);
    }
}