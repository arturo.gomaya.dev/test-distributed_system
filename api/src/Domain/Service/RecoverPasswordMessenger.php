<?php

namespace App\Domain\Service;

use App\Domain\Model\User;

interface RecoverPasswordMessenger
{
    public function send(User $user);
}