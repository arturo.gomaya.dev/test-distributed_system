<?php


namespace App\Domain\Model\Exception;

use Exception;

final class PasswordLoadFromNoHashedString extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function fromString(string $string): self
    {
        return new self(
            \sprintf(
                'String given to load password is not a hashed one: %s',
                $string,
            )
        );
    }
}
