<?php


namespace App\Domain\Model\Exception;


use App\Domain\Model\ValueObject\Email;

final class WrongPassword extends \Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function fromName(Email $email): self
    {
        return new self(
            \sprintf(
                'Wrong password for given user: %s',
                $email->jsonSerialize(),
            )
        );
    }

}