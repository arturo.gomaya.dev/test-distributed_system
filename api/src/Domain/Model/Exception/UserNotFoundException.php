<?php

namespace App\Domain\Model\Exception;

use Exception;

final class UserNotFoundException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function fromName(string $name): self
    {
        return new self(sprintf('User %s not found', $name));
    }
}