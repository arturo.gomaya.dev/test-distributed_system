<?php

namespace App\Domain\Model\ValueObject;

abstract class StringValueObject
{
    protected string $value;

    final protected function __construct(string $value)
    {
        $this->value = $value;
    }

    final public function __toString(): string
    {
        return $this->value;
    }

    final public function equalTo(StringValueObject $other): bool
    {
        return static::class === \get_class($other)
            && $this->value === $other->value;
    }

    final public function jsonSerialize(): string
    {
        return $this->value;
    }

    public static function from(string $value){
        return new static($value);
    }
}