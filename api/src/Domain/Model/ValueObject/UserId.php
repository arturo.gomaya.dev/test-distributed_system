<?php


namespace App\Domain\Model\ValueObject;

class UserId
{
    private string $value;

    final protected function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function from(string $value)
    {
        return new static($value);
    }

    public function jsonSerialize()
    {
        return $this->value;
    }

}