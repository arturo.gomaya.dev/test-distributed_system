<?php

namespace App\Domain\Model\ValueObject;

use App\Domain\Model\Exception\PasswordLoadFromNoHashedString;

class Password extends StringValueObject
{
    const ALGO_NAME_INDEX = 'algoName';
    const BCRYPT = 'bcrypt';

    final public function verify(string $stringPassword): bool
    {
        return password_verify($stringPassword, $this->value);
    }

    final public static function from(string $cryptPassword){
        self::ensureIsHashedPassword($cryptPassword);

        return new static($cryptPassword);
    }

    private static function ensureIsHashedPassword(string $cryptPassword): void
    {
        $cryptInfo = password_get_info($cryptPassword);
        if (self::BCRYPT !== $cryptInfo[self::ALGO_NAME_INDEX]) {
            throw PasswordLoadFromNoHashedString::fromString($cryptPassword);
        }
    }

    final public static function create(string $password)
    {
        $cryptPassword = password_hash($password, PASSWORD_BCRYPT);

        return new static($cryptPassword);
    }
}