<?php

namespace App\Domain\Model;

use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;
use App\Domain\Model\ValueObject\Password;
use App\Domain\Model\ValueObject\UserId;

class User
{
    private UserId $userId;
    private Name $name;
    private Password $password;
    private Email $email;
    private bool $status;

    private function __construct(
        UserId $userId,
        Name $name,
        Password $password,
        Email $email,
        bool $status
    )
    {
        $this->userId = $userId;
        $this->name = $name;
        $this->password = $password;
        $this->email = $email;
        $this->status = $status;
    }

    public static function from(
        UserId $userId,
        Name $name,
        Password $password,
        Email $email,
        bool $status
    ) {
        return new self(
            $userId,
            $name,
            $password,
            $email,
            $status
        );
    }


    public function userId(): UserId
    {
        return $this->userId;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function password(): Password
    {
        return $this->password;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function status(): bool
    {
        return $this->status;
    }

    public function jsonSerialize()
    {
        return[
            'id' => $this->userId,
            'name' => $this->name,
            'password' => $this->password,
            'email' => $this->email,
            'status' => $this->status
        ];
    }
}