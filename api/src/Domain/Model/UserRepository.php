<?php

namespace App\Domain\Model;

use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;

interface UserRepository
{
    public function findByName(Name $name): ?User;
    public function findByEmail(Email $email): ?User;
}