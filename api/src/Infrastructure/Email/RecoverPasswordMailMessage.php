<?php

namespace App\Infrastructure\Email;

class RecoverPasswordMailMessage
{
    private string $message;
    private string $email;

    public function __construct(string $message, string $email)
    {
        $this->message = $message;
        $this->email = $email;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function email(): string
    {
        return $this->email;
    }
}