<?php

namespace App\Infrastructure\Email;

use App\Domain\Model\User;
use App\Domain\Service\RecoverPasswordMessenger;
use Symfony\Component\Messenger\MessageBusInterface;

class RecoverPasswordMailer implements RecoverPasswordMessenger
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }
    public function send(User $user)
    {
        $this->bus->dispatch(new RecoverPasswordMailMessage('Your password is $tuvalum%',(string)$user->email()));
    }
}