<?php

namespace App\Infrastructure\Email;

use Swift_Mailer;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RecoverPasswordMailHandler implements MessageHandlerInterface
{
    private Swift_Mailer $mailer;

    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(RecoverPasswordMailMessage $mailMessage)
    {
        $this->sendMail($mailMessage->message(),$mailMessage->email());
    }

    private function sendMail(string $message, string $email)
    {
        $message = (new \Swift_Message('Recover Password Tuvalum'))
            ->setFrom('copa.del.rejo@gmail.com')
            ->setTo($email)
            ->setBody($message)
        ;

        $this->mailer->send($message);
    }
}