<?php

namespace App\Infrastructure\User;

use App\Domain\Model\User;
use App\Domain\Model\UserRepository;
use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;
use App\Domain\Model\ValueObject\Password;
use App\Domain\Model\ValueObject\UserId;

final class MemoryUserRepository implements UserRepository
{
    private array $collection;

    public function __construct($email)
    {
        $this->collection = [[
            'id'=>"1",
            'name'=>'tuvalum',
            'password'=>(string)Password::create('$tuvalum%'),
            'email'=>$email,
            'status'=>1]
        ];
    }

    public function findByName(Name $name): ?User
    {
        $result = array_filter($this->collection,fn($user) => $user['name'] == $name);
        return count($result) === 0? null:$this->mapToUser($result[0]);
    }

    private function mapToUser(array $user): User
    {
        return User::from(
            UserId::from($user['id']),
            Name::from($user['name']),
            Password::from($user['password']),
            Email::from($user['email']),
            $user['status'],
        );
    }

    public function findByEmail(Email $email): ?User
    {
        $result = array_filter($this->collection,fn($user) => $user['email'] == $email);
        return count($result) === 0? null:$this->mapToUser($result[0]);
    }
}