<?php

namespace App\Application\Authentication\Dto;

use App\Domain\Model\User;
use App\Domain\Model\ValueObject\Name;
use App\Domain\Model\ValueObject\UserId;

final class UserDto implements \JsonSerializable
{
    private UserId $userId;
    private Name $name;

    private function __construct(
        UserId $userId,
        Name $name
    ) {
        $this->userId = $userId;
        $this->name = $name;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->userId->jsonSerialize(),
            'name' => $this->name->jsonSerialize(),
        ];
    }

    public static function fromUser(User $user): UserDto
    {
        return new self(
            $user->userId(),
            $user->name()
        );
    }
}