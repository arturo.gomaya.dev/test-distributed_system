<?php

namespace App\Application\Authentication;

use App\Application\Authentication\Dto\UserDto;
use App\Domain\Service\Authenticator;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AuthenticationHandler implements MessageHandlerInterface
{
    private Authenticator $authenticator;

    public function __construct(Authenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    public function __invoke(AuthenticationQuery $query): UserDto
    {
        $user = $this->authenticator->execute($query->email(),$query->password());

        return UserDto::fromUser($user);
    }
}