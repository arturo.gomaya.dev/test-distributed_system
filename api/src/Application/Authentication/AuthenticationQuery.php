<?php

namespace App\Application\Authentication;

use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;

final class AuthenticationQuery
{
    private Email $email;
    private string $password;

    public function __construct(string $email, string $password)
    {
        $this->email = Email::from($email);
        $this->password = $password;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }
}