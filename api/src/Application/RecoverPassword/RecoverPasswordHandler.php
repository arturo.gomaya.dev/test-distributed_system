<?php

namespace App\Application\RecoverPassword;

use App\Domain\Service\PasswordRecover;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class RecoverPasswordHandler implements MessageHandlerInterface
{
    private PasswordRecover $passwordRecover;

    public function __construct(PasswordRecover $passwordRecover)
    {
        $this->passwordRecover = $passwordRecover;
    }

    public function __invoke(RecoverPasswordCommand $command): void
    {
        $this->passwordRecover->execute($command->username());
    }

}