<?php

namespace App\Application\RecoverPassword;

use App\Domain\Model\ValueObject\Name;

final class RecoverPasswordCommand
{
    private Name $username;

    public function __construct(string $username)
    {
        $this->username = Name::from($username);
    }

    public function username(): Name
    {
        return $this->username;
    }
}