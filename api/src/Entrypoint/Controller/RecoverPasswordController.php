<?php

namespace App\Entrypoint\Controller;

use App\Application\RecoverPassword\RecoverPasswordCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;


final class RecoverPasswordController extends AbstractController
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function __invoke(Request $request): JsonResponse
    {
        try{
            $this->recoverPassword($request);

            return new JsonResponse(null, Response::HTTP_OK);
        }catch(\Exception $e){
            return new JsonResponse(
                ['message'=>$e->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }

    }

    private function recoverPassword(Request $request): void
    {
        $body = new ParameterBag(json_decode($request->getContent(), true));
        $command = new RecoverPasswordCommand($body->get('username'));
        $this->handle($command);
    }
}