<?php

namespace App\Entrypoint\Controller;

use App\Application\Authentication\AuthenticationQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class LoginController extends AbstractController
{
    use HandleTrait;

    private MessageBusInterface $messageBugs;
    private SessionInterface $session;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }
    public function __invoke(Request $request): JsonResponse
    {
        try{
            $user = $this->login($request);
            return new JsonResponse($user, Response::HTTP_OK);
        }catch(\Exception $e){
            return new JsonResponse(
                ['message'=>$e->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    private function login(Request $request): array
    {
        $body = new ParameterBag(json_decode($request->getContent(), true));
        $query = new AuthenticationQuery($body->get('email'),$body->get('password'));

        return $this->handle($query)->jsonSerialize();
    }
}