<?php

namespace App\Tests\Domain\Service;

use App\Domain\Model\Exception\UserNotFoundException;
use App\Domain\Model\Exception\WrongPassword;
use App\Domain\Model\User;
use App\Domain\Model\UserRepository;
use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;
use App\Domain\Model\ValueObject\Password;
use App\Domain\Model\ValueObject\UserId;
use App\Domain\Service\Authenticator;
use PHPUnit\Framework\TestCase;

class AuthenticatorTest extends TestCase
{
    public function test_given_repository_when_create_authenticator_then_return_object(): void
    {
        $repository = $this->createMock(UserRepository::class);
        $result = new Authenticator($repository);

        self::assertInstanceOf(Authenticator::class, $result);
    }

    public function test_given_incorrect_login_data_to_authenticator_when_try_login_then_should_throw_user_not_found_exception(): void
    {
        $repository = $this->createMock(UserRepository::class);
        $repository->expects(self::once())
            ->method('findByEmail')
            ->willReturn(null);
        $authenticator = new Authenticator($repository);
        self::expectException(UserNotFoundException::class);
        $authenticator->execute(Email::from("email"),"");
    }

    public function test_given_incorrect_login_data_to_authenticator_when_try_login_then_should_throw_wrong_password_exception(): void
    {
        $user = User::from(
            UserId::from("1"),
            Name::from("user"),
            Password::create("password"),
            Email::from("email"),
            "1"
        );
        $repository = $this->createMock(UserRepository::class);
        $repository->expects(self::once())
            ->method('findByEmail')
            ->willReturn($user);
        $authenticator = new Authenticator($repository);
        self::expectException(WrongPassword::class);
        $authenticator->execute(Email::from("email"),"");
    }

    public function test_given_correct_login_data_to_authenticator_when_try_login_then_should_return_user(): void
    {
        $user = User::from(
            UserId::from("1"),
            Name::from("user"),
            Password::create("password"),
            Email::from("email"),
            "1"
        );
        $repository = $this->createMock(UserRepository::class);
        $repository->expects(self::once())
            ->method('findByEmail')
            ->willReturn($user);
        $authenticator = new Authenticator($repository);
        $result = $authenticator->execute(Email::from("email"),"password");
        self::assertInstanceOf(User::class, $result);
        self::assertEquals($user,$result);
    }
}