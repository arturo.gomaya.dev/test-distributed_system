<?php

namespace App\Tests\Domain\Service;

use App\Domain\Model\Exception\UserNotFoundException;
use App\Domain\Model\Exception\WrongPassword;
use App\Domain\Model\User;
use App\Domain\Model\UserRepository;
use App\Domain\Model\ValueObject\Email;
use App\Domain\Model\ValueObject\Name;
use App\Domain\Model\ValueObject\Password;
use App\Domain\Model\ValueObject\UserId;
use App\Domain\Service\Authenticator;
use App\Domain\Service\PasswordRecover;
use App\Domain\Service\RecoverPasswordMessenger;
use PHPUnit\Framework\TestCase;

class PasswordRecoverTest extends TestCase
{
    public function test_given_repository_and_messenger_when_create_password_recover_then_return_object(): void
    {
        $repository = $this->createMock(UserRepository::class);
        $messenger = $this->createMock(RecoverPasswordMessenger::class);
        $result = new PasswordRecover($repository,$messenger);

        self::assertInstanceOf(PasswordRecover::class, $result);
    }

    public function test_given_incorrect_username_when_password_recover_then_should_throw_user_not_found_exception(): void
    {
        $repository = $this->createMock(UserRepository::class);
        $messenger = $this->createMock(RecoverPasswordMessenger::class);
        $repository->expects(self::once())
            ->method('findByName')
            ->willReturn(null);
        $passwordRecover = new PasswordRecover($repository,$messenger);
        self::expectException(UserNotFoundException::class);
        $passwordRecover->execute(Name::from("name"));
    }

    public function test_given_correct_data__when_password_recover_then_should_execute_messenger_service(): void
    {
        $user = User::from(
            UserId::from("1"),
            Name::from("user"),
            Password::create("password"),
            Email::from("email"),
            "1"
        );
        $messenger = $this->createMock(RecoverPasswordMessenger::class);
        $repository = $this->createMock(UserRepository::class);
        $repository->expects(self::once())
            ->method('findByName')
            ->willReturn($user);
        $messenger->expects(self::once())
            ->method('send')
            ->with($user);
        $passwordRecover = new PasswordRecover($repository,$messenger);
        $passwordRecover->execute(Name::from("name"));
    }

}