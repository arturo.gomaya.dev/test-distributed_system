DOCKER_COMPOSE := docker-compose -f docker/docker-compose.yml
PHP_UNIT := ./api/vendor/bin/phpunit --bootstrap api/vendor/autoload.php

init:
	make start
	make composer-install
	cd ./app && npm install && npm start
start:
	$(DOCKER_COMPOSE) up --build -d
react:
	cd ./app && npm start
stop:
	$(DOCKER_COMPOSE) down
kill:
	$(DOCKER_COMPOSE) kill
recreate:
	$(DOCKER_COMPOSE) up --build --force-recreate
restart: stop start

#PHP - Composer
composer-install:
	$(DOCKER_COMPOSE) run --rm -u $(UID):$(GID) php composer install -d ./api
composer-update:
	$(DOCKER_COMPOSE) run -u $(UID):$(GID) php composer update -d ./api
composer-require:
ifdef COMPOSERLIBRARY
	$(DOCKER_COMPOSE) exec -u $(UID):$(GID) php composer require -d ./api $(COMPOSERLIBRARY)
endif

composer-remove:
ifdef COMPOSERLIBRARY
	$(DOCKER_COMPOSE) exec -u $(UID):$(GID) php composer remove -d ./api $(COMPOSERLIBRARY)
endif

bash: shell-scripts
shell-scripts:
	$(DOCKER_COMPOSE) exec nginx /bin/sh; exit 0;

phpstan:
	$(DOCKER_COMPOSE) run -T --rm php api/vendor/bin/phpstan analyse -c "api/phpstan.neon"

#PHPUnit:
php-unit-all:
	$(DOCKER_COMPOSE) exec -T -u $(UID):$(GID) php $(PHP_UNIT) --do-not-cache-result api/tests

