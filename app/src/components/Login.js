import React, {PureComponent} from 'react';
import {login,recover} from "../api";

class Login extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            showLogin: true,
            email: '',
            username: '',
            password: '',
            loginMessage: "",
        }
        this.handleRecover = this.handleRecover.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }
    handleChange (field) {
        return (event) => {
            this.setState({
                [field]: event.target.value
            })
        }
    }
    handleRecover(e) {
        e.preventDefault();
        this.setState({showLogin:false});
    }
    handleLogin(e) {
        e.preventDefault();
        this.setState({showLogin:true});
    }
    handleLoginSubmit (e) {
        e.preventDefault();
        login({
            email: this.state.email,
            password: this.state.password,
        }).then((resp) => this.setState({loginMessage: resp}));
    }
    handleRecoverSubmit (e) {
        e.preventDefault();
        recover({
            username: this.state.username,
        }).then((resp) => this.setState({loginMessage: resp}));
    }

    render(){
        const {email, username, password, showLogin, loginMessage} = this.state;
        if(loginMessage !== ''){
            console.log( loginMessage);
            return(
                <div>{loginMessage}</div>
            )
        }
        if(showLogin){
            return(
                <div className="login-content">
                    <div className="login-form">
                        <form>
                            <input type="text" onChange={this.handleChange("email")} placeholder="Email" value={email} required/><br/>
                            <input type="password" onChange={this.handleChange("password")} placeholder="Contraseña" value={password} required/><br/>
                            <input type="submit" onClick={(e) => this.handleLoginSubmit(e)} value="Submit"/>
                        </form><br/>
                        <div className="login-link" onClick={this.handleRecover}>He olvidado mi contraseña</div>
                    </div>
                </div>
            );
        }else{
            return(
                <div className="login-content">
                    <div className="recover-form">
                        <form>
                            <input type="text" onChange={this.handleChange("username")} placeholder="Usuario" value={username} required/><br/>
                            <input type="submit" onClick={(e) => this.handleRecoverSubmit(e)} value="Submit"/>
                        </form><br/>
                        <div className="login-link" onClick={this.handleLogin}>Volver al login</div>
                    </div>
                </div>
            );
        }
    }
}

export default Login;