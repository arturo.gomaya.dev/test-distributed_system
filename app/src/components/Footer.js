import React from "react";

const Footer = () => (
    <div className="footer-content">
        <div className="footer-text">Created By Arturo Gómez</div>
    </div>
);

export default Footer;