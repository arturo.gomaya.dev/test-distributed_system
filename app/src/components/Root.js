import React, {PureComponent} from 'react';
import Header from "./Header";
import Footer from "./Footer";
import Login from "./Login";

class Root extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            videos: null,
            showAdd: false,
        };
    }

    render() {
        return (
            <React.Fragment>
                <Header onClickAdd={this.handleAdd}/>
                <div className="container">
                    <div className="grid-container">
                        <Login />
                    </div>
                </div>
                <Footer/>
            </React.Fragment>
        );
    }
}

export default Root;