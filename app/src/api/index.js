export const login = ({email,password}) => new Promise((resolve, reject) => {
    try{
        fetch('http://api.localhost/login',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
            .then(async response => {
                const data = await response.json();

                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }

                return resolve("Login successful");
            })
            .catch(error => {

                return resolve(error);
            });

        // return resolve(resp);
    }catch(error){
        // return reject(error);
    }
});

export const recover = ({username}) => new Promise((resolve, reject) => {
    try{
        fetch('http://api.localhost/recover',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: username,
            })
        })
            .then(async response => {
                const data = await response.json();

                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }

                return resolve("Proceso completado. Recibirás un correo con tu contraseña");
            })
            .catch(error => {

                return resolve(error);
            });

        // return resolve(resp);
    }catch(error){
        // return reject(error);
    }
});